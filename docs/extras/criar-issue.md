# Criar issue

> [Voltar para a Página Principal](../README.md)

Crie um tópico para relatar dúvidas e erros.

![001](imagens/criar-issue-1.png)
![002](imagens/criar-issue-2.png)
![003](imagens/criar-issue-3.png)
![004](imagens/criar-issue-4.png)
![005](imagens/criar-issue-5.png)
