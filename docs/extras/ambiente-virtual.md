# Ambiente virtualizado do Python

> [Voltar para a Página Principal](../README.md)

![Imagem representando certo/errado](imagens/representacao-ambiente-virtual.png)

## Terminal

![Imagem auto-explicativa para criar venv no terminal](imagens/criar-venv-python-terminal.png)

## Pycharm

### Ao criar um projeto

![Imagem auto-explicativa para criar projeto no pycharm com venv](imagens/criar-projeto-python-venv-pycharm.png)

### Em um projeto já existente

![Imagem auto-explicativa para ir para o settings do pycharm](imagens/criar-venv-pycharm-projeto-1.png)

![Imagem auto-explicativa para ir para o interpretador do python no pycharm](imagems/criar-venv-pycharm-projeto-2.png)

![Imagem auto-explicativa para criar venv no pycharm](imagens/criar-venv-pycharm-projeto-3.png)
