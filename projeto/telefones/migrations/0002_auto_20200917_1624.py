# Generated by Django 3.1.1 on 2020-09-17 19:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('telefones', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telefone',
            name='codigo_discagem',
            field=models.CharField(choices=[('83', 'PARAIBA'), ('81', 'PERNAMBUCO'), ('84', 'RIO GRANDE DO NORTE')], default='83', max_length=2),
        ),
    ]
