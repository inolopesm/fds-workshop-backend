from django.db import models
from django.utils.translation import gettext_lazy as _

class Telefone(models.Model):
    class CodigoDiscagemPorEstado(models.TextChoices):
        PARAIBA = '83', _('PARAIBA')
        PERNAMBUCO = '81', _('PERNAMBUCO')
        RIO_GRANDE_DO_NORTE = '84', _('RIO GRANDE DO NORTE')

    codigo_discagem = models.CharField(
        max_length=2, choices=CodigoDiscagemPorEstado.choices, default=CodigoDiscagemPorEstado.PARAIBA
    )
    numero = models.CharField(max_length=9)
    pessoa = models.ForeignKey('pessoas.Pessoa', on_delete=models.CASCADE)

    def __str__(self):
        return '({}) {}'.format(self.codigo_discagem, self.numero)