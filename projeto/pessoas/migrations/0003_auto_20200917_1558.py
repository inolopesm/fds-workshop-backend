# Generated by Django 3.1.1 on 2020-09-17 18:58

from django.db import migrations, models
import pessoas.models


class Migration(migrations.Migration):

    dependencies = [
        ('pessoas', '0002_pessoa_foto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='foto',
            field=models.ImageField(null=True, upload_to=pessoas.models.personaliza_foto),
        ),
    ]
