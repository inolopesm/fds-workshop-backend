from django.db import models

# MODELO QUE REPRESENTARÁ UMA TABELA NO BANCO DE DADOS
# CADA CRIAÇÃO/ALTERAÇÃO AQUI DEVEMOS GERAR UM NOVO COMPILADO PARA AVISAR AO BANCO DE DADOS, UTILIZANDO
# python manage.py makemigratios
# python manage.py migrate

def personaliza_foto(instance, filename):
    ext = filename.split('.')[-1]
    return 'pessoas/{}.{}'.format(instance.nome, ext)

class Pessoa(models.Model):
    nome = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    idade = models.IntegerField()
    foto = models.ImageField(null=True, upload_to=personaliza_foto)
    amigos = models.ManyToManyField('self', blank=True)

    def __str__(self):
        return '{} {}'.format(self.id, self.nome)
