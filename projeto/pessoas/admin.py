from django.contrib import admin
from .models import Pessoa
from .forms import PessoaForm

# Register your models here.
class PessoaAdmin(admin.ModelAdmin):
    form = PessoaForm

admin.site.register(Pessoa, PessoaAdmin)